package models

import (
	"log"
)

/*
	Esta clase contiene el modelo para el dispositivo
*/
type Dispositivo struct {
	IDDispositivo int     `db:"id_dispositivo"`
	Mac           string  `db:"mac"`
	FechaCompra   []uint8 `db:"fecha_compra"`
	Version       string  `db:"version"`
	Costo         float32 `db:"costo"`
	Activo        int     `db:"activo"`
}

/*
	GetDispositivo:
	Este metodo retorna los datos de un dispositivo por id
*/
func (dis *Dispositivo) GetDispositivo(id int) (Dispositivo, error) {
	sql := `SELECT id_dispositivo, mac, fecha_compra, 
	version, costo, activo FROM dispositivo WHERE
	id_dispositivo = ?`
	dispositivo := Dispositivo{}
	err := db.Get(&dispositivo, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar dispositivo: ")
		log.Println(err)
	}
	return dispositivo, err
}

/*
	GetDispositivos:
	Este metodo retorna los datos de todos los dispositivos de la db
*/
func (dis *Dispositivo) GetDispositivos() ([]Dispositivo, error) {
	sql := `SELECT id_dispositivo, mac, fecha_compra, version, 
	costo, activo FROM dispositivo
	ORDER BY id_dispositivo DESC`

	dispositivos := []Dispositivo{}
	err := db.Select(&dispositivos, sql)
	log.Println(dispositivos)
	if err != nil {
		log.Print("ERROR al encontrar dispositivos: ")
		log.Println(err)
	}
	return dispositivos, err
}
