package models

import "log"

/*
	Esta clase contiene el modelo para el nivel de estudio
*/
type NivelEstudio struct {
	IDNivelEstudio int    `db:"id_nivel_estudio"`
	Nombre         string `db:"nombre"`
	Descripcion    string `db:"descripcion"`
}

/*
	GetNivelEstudio:
	Este metodo retorna los datos de un nivel de estudio por id
*/
func (ne *NivelEstudio) GetNivelEstudio(id int) (NivelEstudio, error) {
	sql := `SELECT id_nivel_estudio, nombre, descripcion FROM nivel_estudio 
	WHERE id_nivel_estudio = ?`
	nivelEstudio := NivelEstudio{}
	err := db.Get(&nivelEstudio, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar el nivel estudio: ")
		log.Println(err)
	}
	return nivelEstudio, err
}

/*
	GetNivelesEstudio:
	Este metodo retorna todos los datos de los niveles de estudio de la db
*/
func (ne *NivelEstudio) GetNivelesEstudio() ([]NivelEstudio, error) {
	sql := `SELECT id_nivel_estudio, nombre, descripcion FROM nivel_estudio 
	ORDER BY id_nivel_estudio DESC`

	nivelesEstudio := []NivelEstudio{}
	err := db.Select(&nivelesEstudio, sql)
	log.Println(nivelesEstudio)
	if err != nil {
		log.Print("ERROR al encontrar niveles de estudio: ")
		log.Println(err)
	}
	return nivelesEstudio, err
}
