package models

import "log"

/*
	Esta clase tiene el modelo para la tabla perfil
*/
type Perfil struct {
	IDPerfil    int    `db:"id_perfil"`
	Nombre      string `db:"nombre"`
	Descripcion string `db:"descripcion"`
}

/*
	GetPerfil:
	Este metodo retorna los datos de un tipo de perfil por id
*/
func (per Perfil) GetPerfil(id int) (Perfil, error) {
	sql := `SELECT id_perfil, nombre, descripcion FROM perfil WHERE
	id_perfil = ?`
	perfil := Perfil{}
	err := db.Get(&perfil, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar perfil: ")
		log.Println(err)
	}
	return perfil, err
}

/*
	GetPerfiles:
	Este metodo retorna la informacion de todos los tipos de perfiles de la db
*/
func (per Perfil) GetPerfiles() ([]Perfil, error) {
	sql := `SELECT id_perfil, nombre, descripcion, nivel FROM perfil 
	ORDER BY id_perfil DESC`

	perfiles := []Perfil{}
	err := db.Select(&perfiles, sql)
	log.Println(perfiles)
	if err != nil {
		log.Print("ERROR al encontrar perfiles: ")
		log.Println(err)
	}
	return perfiles, err
}
