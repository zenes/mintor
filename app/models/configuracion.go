package models

import "log"

/*
Esta clase contiene el modelo para la configuracion
*/
type Configuracion struct {
	IDConfiguracion int    `db:"id_configuracion"`
	APIId           string `db:"api_id"`
	Intervalo       int    `db:"intervalo"`
	Email           string `db:"email"`
	activo          int    `db:"activo"`
}

/*
	GetConf:
	Este metodo retorna la informacion basica de la configuracion del servidor
*/
func getConf() (Configuracion, error) {
	id := 1
	sql := `SELECT id_configuracion, api_id, intervalo, email, activo 
	FROM configuracion WHERE id_configuracion = ?`

	configuracion := Configuracion{}
	err := db.Get(&configuracion, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar configuracion: ")
		log.Println(err)
	}
	return configuracion, err
}
