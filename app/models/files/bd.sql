-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema hackathon
-- -----------------------------------------------------
-- Base de datos para prototipo hackathon zacatecas 2019

-- -----------------------------------------------------
-- Schema hackathon
--
-- Base de datos para prototipo hackathon zacatecas 2019
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hackathon` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
USE `hackathon` ;

-- -----------------------------------------------------
-- Table `hackathon`.`genero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`genero` (
  `id_genero` INT(2) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(63) NOT NULL,
  PRIMARY KEY (`id_genero`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`nivel_estudio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`nivel_estudio` (
  `id_nivel_estudio` INT(2) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(63) NOT NULL,
  `descripcion` VARCHAR(255) NULL DEFAULT 'N-A',
  PRIMARY KEY (`id_nivel_estudio`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`perfil` (
  `id_perfil` INT(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(63) NOT NULL,
  `descripcion` VARCHAR(255) NULL,
  PRIMARY KEY (`id_perfil`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`cargo` (
  `id_cargo` INT(2) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(63) NOT NULL,
  `descripcion` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_cargo`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE,
  UNIQUE INDEX `descripcion_UNIQUE` (`descripcion` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`contratista`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`contratista` (
  `id_contratista` INT(3) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(63) NOT NULL,
  `descripcion` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_contratista`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE,
  UNIQUE INDEX `descripcion_UNIQUE` (`descripcion` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`dispositivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`dispositivo` (
  `id_dispositivo` INT(63) NOT NULL AUTO_INCREMENT,
  `mac` VARCHAR(127) NOT NULL,
  `fecha_compra` DATETIME NOT NULL DEFAULT NOW(),
  `version` VARCHAR(4) NOT NULL DEFAULT '1',
  `costo` FLOAT(31) NOT NULL,
  `activo` INT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_dispositivo`),
  UNIQUE INDEX `mac_UNIQUE` (`mac` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`area_trabajo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`area_trabajo` (
  `id_area_trabajo` INT(15) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(125) NOT NULL,
  `descripcion` VARCHAR(255) NOT NULL,
  `nivel` INT(3) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_area_trabajo`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`usuario` (
  `id_usuario` INT(127) NOT NULL AUTO_INCREMENT,
  `id_genero` INT(2) NOT NULL,
  `id_nivel_estudio` INT(2) NOT NULL,
  `id_perfil` INT(1) NOT NULL,
  `id_cargo` INT(2) NOT NULL,
  `id_contratista` INT(3) NOT NULL,
  `id_dispositivo` INT(63) NOT NULL,
  `id_area_trabajo` INT(15) NOT NULL,
  `nombre` VARCHAR(63) NOT NULL,
  `apellido` VARCHAR(63) NOT NULL,
  `email` VARCHAR(63) NOT NULL,
  `fecha_nac` DATE NOT NULL,
  `telefono` VARCHAR(31) NOT NULL,
  `pass` VARCHAR(127) NOT NULL,
  `usuario` VARCHAR(31) NOT NULL,
  `lugar_nacimiento` VARCHAR(255) NOT NULL,
  `lugar_residencia` VARCHAR(255) NOT NULL,
  `personas_cargo` INT(2) NOT NULL DEFAULT 0,
  `foto` TEXT NULL,
  `activo` INT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_usuario`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_usuario_genero_idx` (`id_genero` ASC) VISIBLE,
  INDEX `fk_usuario_nivel_estudios1_idx` (`id_nivel_estudio` ASC) VISIBLE,
  INDEX `fk_usuario_perfil1_idx` (`id_perfil` ASC) VISIBLE,
  UNIQUE INDEX `telefono_UNIQUE` (`telefono` ASC) VISIBLE,
  UNIQUE INDEX `pass_UNIQUE` (`pass` ASC) VISIBLE,
  UNIQUE INDEX `usuario_UNIQUE` (`usuario` ASC) VISIBLE,
  INDEX `fk_usuario_cargo1_idx` (`id_cargo` ASC) VISIBLE,
  INDEX `fk_usuario_contratista1_idx` (`id_contratista` ASC) VISIBLE,
  INDEX `fk_usuario_dispositivo1_idx` (`id_dispositivo` ASC) VISIBLE,
  INDEX `fk_usuario_lugar1_idx` (`id_area_trabajo` ASC) VISIBLE,
  CONSTRAINT `fk_usuario_genero`
    FOREIGN KEY (`id_genero`)
    REFERENCES `hackathon`.`genero` (`id_genero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_nivel_estudios1`
    FOREIGN KEY (`id_nivel_estudio`)
    REFERENCES `hackathon`.`nivel_estudio` (`id_nivel_estudio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_perfil1`
    FOREIGN KEY (`id_perfil`)
    REFERENCES `hackathon`.`perfil` (`id_perfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_cargo1`
    FOREIGN KEY (`id_cargo`)
    REFERENCES `hackathon`.`cargo` (`id_cargo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_contratista1`
    FOREIGN KEY (`id_contratista`)
    REFERENCES `hackathon`.`contratista` (`id_contratista`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_dispositivo1`
    FOREIGN KEY (`id_dispositivo`)
    REFERENCES `hackathon`.`dispositivo` (`id_dispositivo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_lugar1`
    FOREIGN KEY (`id_area_trabajo`)
    REFERENCES `hackathon`.`area_trabajo` (`id_area_trabajo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`deteccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`deteccion` (
  `id_deteccion` INT(255) NOT NULL,
  `id_dispositivo` INT(63) NOT NULL,
  `CO` VARCHAR(5) NOT NULL DEFAULT '0',
  `CO2` VARCHAR(5) NOT NULL DEFAULT '0',
  `metano` VARCHAR(5) NOT NULL DEFAULT '0',
  `nitrogeno` VARCHAR(5) NOT NULL DEFAULT '0',
  `temp` VARCHAR(5) NOT NULL DEFAULT '0' COMMENT 'grados C\n',
  `humedad` VARCHAR(5) NOT NULL DEFAULT '0',
  `fecha` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id_deteccion`),
  INDEX `fk_deteccion_dispositivo1_idx` (`id_dispositivo` ASC) VISIBLE,
  CONSTRAINT `fk_deteccion_dispositivo1`
    FOREIGN KEY (`id_dispositivo`)
    REFERENCES `hackathon`.`dispositivo` (`id_dispositivo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`configuracion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`configuracion` (
  `id_configuracion` INT NOT NULL,
  `api_id` VARCHAR(127) NOT NULL,
  `intervalo` INT(3) NOT NULL DEFAULT 10 COMMENT 'SEGUNDOS\n',
  `email` VARCHAR(63) NOT NULL,
  `activo` INT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_configuracion`),
  UNIQUE INDEX `api_id_UNIQUE` (`api_id` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hackathon`.`modulo_wifi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hackathon`.`modulo_wifi` (
  `id_modulo_wifi` INT(63) NOT NULL AUTO_INCREMENT,
  `id_area_trabajo` INT(15) NOT NULL,
  `mac` VARCHAR(45) NULL,
  PRIMARY KEY (`id_modulo_wifi`),
  INDEX `fk_modulo_wifi_area_trabajo1_idx` (`id_area_trabajo` ASC) VISIBLE,
  CONSTRAINT `fk_modulo_wifi_area_trabajo1`
    FOREIGN KEY (`id_area_trabajo`)
    REFERENCES `hackathon`.`area_trabajo` (`id_area_trabajo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
