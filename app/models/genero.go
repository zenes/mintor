package models

import (
	"log"

	"github.com/revel/revel"
)

/*
	Esta clase contiene el modelo para el genero
*/
type Genero struct {
	IDGenero int    `db:"id_genero"`
	Nombre   string `db:"nombre"`
}

/*
	GetGenero:
	Este metodo retorna la información de un solo genero por id
*/

/*
	ValidaNuevoGenero:
	Este metodo realiza la validacion de los campos obligatorios para insertar un nuevo genero
*/
func (gen *Genero) ValidaNuevoGenero(v *revel.Validation) {
	v.Required(gen.Nombre)
}

/*
	GetGenero:
	Este metodo selecciona la informacion de un genero por id
*/
func (gen *Genero) GetGenero(id int) (Genero, error) {
	sql := `SELECT id_genero, nombre FROM genero WHERE
	id_genero = ?`
	genero := Genero{}
	err := db.Get(&genero, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar genero: ")
		log.Println(err)
	}
	return genero, err
}

/*
	GetGeneros:
	Este metodo retorna todos los generos agregados en la db
*/
func (gen Genero) GetGeneros() ([]Genero, error) {
	sql := `SELECT id_genero, nombre FROM genero 
	ORDER BY id_genero DESC`

	generos := []Genero{}
	err := db.Select(&generos, sql)
	log.Println(generos)
	if err != nil {
		log.Print("ERROR al encontrar generos: ")
		log.Println(err)
	}
	return generos, err
}
