package models

/*
	Esta clase contiene la información del usuario
*/

import (
	"log"
	"regexp"

	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
)

type Usuario struct {
	// ID's
	IDUsuario      int `db:"id_usuario"`
	IDGenero       int `db:"id_genero"`
	IDPerfil       int `db:"id_perfil"`
	IDCargo        int `db:"id_cargo"`
	IDContratista  int `db:"id_contratista"`
	IDDispositivo  int `db:"id_dispositivo"`
	IDNivelEstudio int `db:"id_nivel_estudio"`
	IDAreaTrabajo  int `db:"id_area_trabajo"`
	// Campos
	Nombre          string  `db:"nombre"`
	Apellido        string  `db:"apellido"`
	Email           string  `db:"email"`
	FechaNac        []uint8 `db:"fecha_nac"`
	Telefono        string  `db:"telefono"`
	Pass            string  `db:"pass"`
	Usuario         string  `db:"usuario"`
	LugarNacimiento string  `db:"lugar_nacimiento"`
	LugarResidencia string  `db:"lugar_residencia"`
	PersonasCargo   int     `db:"personas_cargo"`
	Foto            *string `db:"foto"`
	Activo          int     `db:"activo"`
	// Herencias
	Genero       Genero
	Contratista  Contratista
	Cargo        Cargo
	Perfil       Perfil
	NivelEstudio NivelEstudio
	AreaTrabajo  AreaTrabajo
	Dispositivo  Dispositivo
}

/*
	ValidarNuevoUsuario:
	Este metodo valida los datos cuando se intenta crear un nuevo usuario
*/
func (us *Usuario) ValidarNuevoUsuario(v *revel.Validation) {
	v.Required(us.IDGenero)
	v.Required(us.IDCargo)
	v.Required(us.IDPerfil)
	v.Required(us.IDNivelEstudio)
	v.Required(us.IDAreaTrabajo)
	v.Required(us.IDDispositivo)
	v.Required(us.IDContratista)
	v.Required(us.Nombre)
	v.Required(us.Apellido)
	v.Required(us.Email)
	v.Required(us.Telefono)
	v.Required(us.Pass)
	v.Required(us.Usuario)
	v.Required(us.LugarNacimiento)
	v.Required(us.LugarResidencia)
	v.Match(us.Usuario, regexp.MustCompile("^\\w*$"))
	v.MinSize(us.Pass, 6).Message("La contraseña debe tener al menos 6 caracteres")
	v.MaxSize(us.Pass, 15).Message("La contraseña no puede tener mas de 15 caracteres")
}

/*
	ValidarInicioSesion:
	Este metodo valida que los campos de inicio de sesion esten correctamente rellenados
*/
func (us *Usuario) ValidarInicioSesion(v *revel.Validation) error {
	v.Required(us.Email).Message("Debe introducirse el correo electronico")
	v.Required(us.Pass).Message("Por favor introducir la contraseña")
	v.MinSize(us.Pass, 6).Message("La contraseña debe tener al menos 6 caracteres")
	v.MaxSize(us.Pass, 15).Message("La contraseña no puede tener mas de 15 caracteres")
	if v.HasErrors() {
		return nil
	}
	sql := `SELECT id_perfil, email, pass, foto, usuario 
	FROM usuario WHERE activo = 1 AND email = ?`
	usuario := Usuario{}
	err := db.Get(&usuario, sql, us.Email)
	if err != nil {
		return err
	}
	err = bcrypt.CompareHashAndPassword([]byte(usuario.Pass), []byte(us.Pass))
	if err == nil {
		usuario.Pass = ""
		*us = usuario
	}
	return err
}

/*
	GetUser:
	Este metodo retorna los datos de un usuario por id
*/
func (us *Usuario) GetUsuario(id int) (*Usuario, error) {
	sql := `SELECT id_usuario, id_genero,
	id_nivel_estudio, id_perfil, id_cargo, 
	id_contratista, id_dispositivo, id_area_trabajo,
	nombre, apellido, email, fecha_nac, telefono,
	pass, usuario, lugar_nacimiento, lugar_residencia, personas_cargo,
	activo, foto FROM usuario WHERE id_usuario = ?`

	err := db.Get(&us, sql, id)
	us.Genero, err = us.Genero.GetGenero(us.IDGenero)
	us.Cargo, err = us.Cargo.GetCargo(us.IDCargo)
	us.Contratista, err = us.Contratista.GetContratista(us.IDContratista)
	us.Dispositivo, err = us.Dispositivo.GetDispositivo(us.IDDispositivo)
	us.NivelEstudio, err = us.NivelEstudio.GetNivelEstudio(us.IDNivelEstudio)
	us.AreaTrabajo, err = us.AreaTrabajo.GetAreaTrabajo(us.IDAreaTrabajo)
	us.Perfil, err = us.Perfil.GetPerfil(us.IDPerfil)
	if err != nil {
		log.Print("ERROR al encontrar usuario: ")
		log.Println(err)
	}
	return us, err
}

/*
	GetUsuarios:
	Este metodo retorna los datos de todos los usuarios de la db
*/
func (us *Usuario) GetUsuarios() ([]Usuario, error) {
	sql := `SELECT id_usuario, id_genero,
	id_nivel_estudio, id_perfil, id_cargo, 
	id_contratista, id_dispositivo, id_area_trabajo,
	nombre, apellido, email, fecha_nac, telefono,
	pass, usuario, lugar_nacimiento, lugar_residencia, personas_cargo,
	activo FROM usuario ORDER BY id_usuario DESC`

	usuarios := []Usuario{}
	err := db.Select(&usuarios, sql)
	for i := range usuarios {
		usuarios[i].Genero, err = usuarios[i].Genero.GetGenero(usuarios[i].IDGenero)
		usuarios[i].Cargo, err = usuarios[i].Cargo.GetCargo(usuarios[i].IDCargo)
		usuarios[i].Contratista, err = usuarios[i].Contratista.GetContratista(usuarios[i].IDContratista)
		usuarios[i].Dispositivo, err = usuarios[i].Dispositivo.GetDispositivo(usuarios[i].IDDispositivo)
		usuarios[i].NivelEstudio, err = usuarios[i].NivelEstudio.GetNivelEstudio(usuarios[i].IDNivelEstudio)
		usuarios[i].AreaTrabajo, err = usuarios[i].AreaTrabajo.GetAreaTrabajo(usuarios[i].IDNivelEstudio)
		usuarios[i].Perfil, err = usuarios[i].Perfil.GetPerfil(usuarios[i].IDPerfil)
	}
	if err != nil {
		log.Print("ERROR al encontrar los usuarios: ")
		log.Println(err)
		return usuarios, err
	}
	return usuarios, err
}

/*
	NuevoUsuario:
	Este metodo se encarga de insertar un nuevo usuario a la base de datos y retorna
	el error en caso de existir
*/
func (us *Usuario) NuevoUsuario() error {
	var err error
	tx := db.MustBegin()
	err, us.Pass = PassHash(us.Pass)
	if err != nil {
		return err
	}
	sql := `INSERT INTO usuario (id_genero, id_nivel_estudio, 
		id_perfil, id_cargo, id_contratista, id_dispositivo, id_area_trabajo, 
		nombre, apellido, email, fecha_nac, telefono, pass, usuario, lugar_nacimiento, 
		lugar_residencia, personas_cargo, foto) VALUES (:id_genero, :id_nivel_estudio, :id_perfil, 
			:id_cargo, :id_contratista, :id_dispositivo, :id_area_trabajo, :nombre, :apellido, 
			:email, :fecha_nac, :telefono, :pass, :usuario, :lugar_nacimiento, :lugar_residencia, :personas_cargo, :foto)`
	_, err = tx.NamedExec(sql, &us)
	if err != nil {
		log.Print("ERROR AL INGRESAR NUEVO USUARIO")
		return err
	}
	tx.Commit()
	return err
}
