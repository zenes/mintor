package models

import "log"

/*
	Esta clase tiene el modelo para la tabla cargo
*/
type Cargo struct {
	IDCargo     int    `db:"id_cargo"`
	Nombre      string `db:"nombre"`
	Descripcion string `db:"descripcion"`
}

/*
	GetCargo:
	Este metodo retorna la informacion de un cargo por id
*/
func (car *Cargo) GetCargo(id int) (Cargo, error) {
	sql := `SELECT id_cargo, nombre, descripcion FROM cargo WHERE
	id_cargo = ?`
	cargo := Cargo{}
	err := db.Get(&cargo, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar cargo: ")
		log.Println(err)
	}
	return cargo, err
}

/*
	GetCargos:
	Este metodo retorna todos los datos de los cargos agregados en la db
*/
func (car *Cargo) GetCargos() ([]Cargo, error) {
	sql := `SELECT id_cargo, nombre, descripcion FROM cargo 
	ORDER BY id_cargo DESC`

	cargos := []Cargo{}
	err := db.Select(&cargos, sql)
	log.Println(cargos)
	if err != nil {
		log.Print("ERROR al encontrar cargos: ")
		log.Println(err)
	}
	return cargos, err
}
