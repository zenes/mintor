package models

import "log"

/*
	Esta clase contiene la informacion de los modulos wifi
*/

type ModuloWifi struct {
	/*
		ID's
	*/
	IDModuloWifi  int    `db:"id_modulo_wifi"`
	IDAreaTrabajo int    `db:"id_area_trabajo"`
	Mac           string `db:"mac"`
}

/*
	Este metodo retorna la informacion de un modulo de wifi por id
*/
func (mod *ModuloWifi) GetModuloWifi(id int) (*ModuloWifi, error) {
	sql := `SELECT id_modulo_wifi, id_area_trabajo, mac FROM modulo_wifi 
	WHERE id_modulo_wifi = ?`

	err := db.Get(&mod, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar modulo wifi")
		log.Println(err)
	}
	return mod, err
}

/*
	Este metodo retorna todos los datos de todos los modulsod e wifi
*/
func (mod *ModuloWifi) GetModulosWifi() ([]ModuloWifi, error) {
	sql := `SELECT id_modulo_wifi, id_area_trabajo, mac 
	FROM modulo_wifi`
	modulos := []ModuloWifi{}
	err := db.Select(&modulos, sql)
	if err != nil {
		log.Print("Error al encontrar los modulos de wifi")
		log.Println(err)
	}
	return modulos, err
}

/*
	Este metodo retorna los modulos wifi de un area de trabajo
*/
func (mod *ModuloWifi) GetModulosWifiAt(idAt int) ([]ModuloWifi, error) {
	sql := `SELECT id_modulo_wifi, id_area_trabajo, mac 
	FROM modulo_wifi WHERE id_area_trabajo = ?`
	modulos := []ModuloWifi{}
	err := db.Select(&modulos, sql, idAt)
	if err != nil {
		log.Print("Error al encontrar los modulos de wifi")
		log.Println(err)
	}
	return modulos, err
}
