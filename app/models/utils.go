package models

import (
	"log"

	"github.com/coopernurse/gorp"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"
)

// Global database references
var db *sqlx.DB
var dbmap *gorp.DbMap

// Database settings
const (
	db_name = "hackathon"
	db_user = "local"
	db_pw   = "jok200"
)

// Create database connection
func InitDB() {
	var err error

	db, err = sqlx.Open("mysql", db_user+":"+db_pw+"@tcp(127.0.0.1:3306)/"+db_name+"?parseTime=false")

	if err != nil {
		log.Println("Failed to connect to database: ")
		log.Panic(err)
	} else {
		err = db.Ping()

		if err != nil {
			log.Println("Failed to ping database: ")
			log.Panic(err)
		} else {
			log.Println("Database connected.")
		}
	}
}

/*
	PassHash:
	Esta funcion genera un nuevo hash para una contraseña y lo retorna ademas de un posible error
*/
func PassHash(pass string) (error, string) {
	hash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.MinCost)
	if err != nil {
		return err, ""
	}
	return err, string(hash)
}
