package models

import "log"

/*
	Esta clase contiene el modelo para el area de trabajo
*/
type AreaTrabajo struct {
	/*
		ID's
	*/
	IDAreaTrabajo int    `db:"id_area_trabajo"`
	Nombre        string `db:"nombre"`
	Descripcion   string `db:"descripcion"`
	Nivel         int    `db:"nivel"`
	/*
		Herencias
	*/
	ModulosWifi []ModuloWifi
}

/*
	GetAreaTrabajo:
	Este metodo retorna la informacion de un area de trabajo por id
*/
func (at *AreaTrabajo) GetAreaTrabajo(id int) (AreaTrabajo, error) {
	sql := `SELECT id_area_trabajo, nombre, descripcion, nivel 
	FROM area_trabajo 
	WHERE id_area_trabajo = ?`
	err := db.Get(&at, sql, id)
	areaTrabajo := AreaTrabajo{}
	mod := ModuloWifi{}
	at.ModulosWifi, err = mod.GetModulosWifiAt(areaTrabajo.IDAreaTrabajo)
	if err != nil {
		log.Print("ERROR al encontrar el nivel estudio: ")
		log.Println(err)
	}
	return areaTrabajo, err
}

/*
	GetAreasTrabajo:
	Este metodo es la encargada de generar un arreglo con todas las
	areas de trabajo
*/
func (at *AreaTrabajo) GetAreasTrabajo() ([]AreaTrabajo, error) {
	sql := `SELECT id_area_trabajo, nombre, descripcion, nivel FROM area_trabajo 
	ORDER BY id_area_trabajo DESC`

	areasTrabajo := []AreaTrabajo{}
	err := db.Select(&areasTrabajo, sql)
	log.Println(areasTrabajo)
	if err != nil {
		log.Print("ERROR al encontrar areas de trabajo: ")
		log.Println(err)
	}
	return areasTrabajo, err
}
