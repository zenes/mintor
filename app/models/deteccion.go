package models

import (
	"log"
	"time"
)

/*
	Esta clase contiene el modelo para las detecciones
*/
type Deteccion struct {
	// ID'
	IDDeteccion   int `db:"id_deteccion"`
	IDDispositivo int `db:"id_dispositivo"`
	// Datps
	CO        string    `db:"CO"`
	CO2       string    `db:"CO2"`
	Metano    string    `db:"metano"`
	Nitrogeno string    `db:"nitrogeno"`
	Temp      string    `db:"temp"`
	Humedad   string    `db:"humedad"`
	Fecha     time.Time `db:"fecha"`

	// Herencias
	Dispositivo Dispositivo
}

/*
	GetDeteccion:
	Este metodo retona los datos de 1 lectura  por id
*/
func (det *Deteccion) GetDeteccion(id int) (Deteccion, error) {
	sql := `SELECT id_deteccion, CO, CO2, metano, nitrogeno, temp, humedad, fecha 
	FROM usuario WHERE id_deteccion = ?`

	deteccion := Deteccion{}
	err := db.Get(&deteccion, sql, id)
	deteccion.Dispositivo, err = det.Dispositivo.GetDispositivo(deteccion.IDDispositivo)
	if err != nil {
		log.Print("ERROR al encontrar deteccion: ")
		log.Println(err)
	}
	return deteccion, err
}

/*
	Este metodo retorna los valores de todas las detecciones encontradas en la db
*/
func (det *Deteccion) GetDetecciones() ([]Deteccion, error) {
	sql := `SELECT id_deteccion, CO, CO2, metano, nitrogeno, temp, humedad, fecha 
	FROM usuario WHERE id_deteccion = ?`

	detecciones := []Deteccion{}
	err := db.Select(&detecciones, sql)
	log.Println(detecciones)
	if err != nil {
		log.Print("ERROR al encontrar detecciones: ")
		log.Println(err)
	}
	return detecciones, err
}
