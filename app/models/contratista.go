package models

import "log"

/*
	Esta clase contiene el modelo para el contratista
*/
type Contratista struct {
	IDContratista int    `db:"id_contratista"`
	Nombre        string `db:"nombre"`
	Descripcion   string `db:"descripcion"`
}

/*
	GetContratista:
	Este metodo retorna la informacion de un contratista por id
*/
func (con Contratista) GetContratista(id int) (Contratista, error) {
	sql := `SELECT id_contratista, nombre, descripcion 
	FROM contratista WHERE
	id_contratista = ?`
	contratista := Contratista{}
	err := db.Get(&contratista, sql, id)
	if err != nil {
		log.Print("ERROR al encontrar contratista: ")
		log.Println(err)
	}
	return contratista, err
}

/*
	GetContratistas:
	Este metodo retorna los datos de todos los contratistasd de la db
*/
func (con Contratista) GetContratistas() ([]Contratista, error) {
	sql := `SELECT id_contratista, nombre, descripcion FROM contratista 
	ORDER BY id_contratista DESC`

	contratistas := []Contratista{}
	err := db.Select(&contratistas, sql)
	log.Println(contratistas)
	if err != nil {
		log.Print("ERROR al encontrar contratistas: ")
		log.Println(err)
	}
	return contratistas, err
}
