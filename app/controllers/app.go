package controllers

import (
	"mincrow/app/models"

	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
}

func (c App) checkUser() revel.Result {
	if c.Session["usuario"] == nil {
		c.Flash.Error("Para continuar primero debes iniciar sesion")
		return c.Redirect((*App).Index)
	}
	return nil
}

/*
	Esta funcion retorna el index de la pagina
*/
func (c App) Index(usu *models.Usuario) revel.Result {
	pagina := revel.AppName + " - Index"
	if c.Session["usuario"] != nil {
		c.Redirect((*User).Home)
	}
	// t := []uint8("2019-07-07")
	// usu.IDGenero = 1
	// usu.IDPerfil = 1
	// usu.IDCargo = 1
	// usu.IDContratista = 1
	// usu.IDDispositivo = 1
	// usu.IDNivelEstudio = 1
	// usu.IDAreaTrabajo = 1
	// usu.Nombre = "Juan"
	// usu.Apellido = "Jimenez"
	// usu.Email = "jmilo@protonmail.com"
	// usu.FechaNac = t
	// usu.Telefono = "+52 551323123123"
	// usu.Pass = "clave-123"
	// usu.Usuario = "user1"
	// usu.LugarNacimiento = "N-A"
	// usu.LugarResidencia = "N-A"
	// usu.ValidarNuevoUsuario(c.Validation)
	// var err error
	// // Handle errors
	// if c.Validation.HasErrors() {
	// 	c.Validation.Keep()
	// 	c.FlashParams()
	// 	log.Println("Error al validar usuario")
	// } else {
	// 	err = usu.NuevoUsuario()
	// 	if err != nil {
	// 		log.Print(err)
	// 	}
	// }
	// usu, err = usu.GetUsuario(1)
	return c.Render(usu, pagina)
}
