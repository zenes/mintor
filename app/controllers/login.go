package controllers

import (
	"mincrow/app/models"
	"net/http"
	"time"

	"github.com/go-playground/form"
	"github.com/revel/revel"
)

type Actions struct {
	*revel.Controller
}

/*
	Esta funcion retorna el index de la pagina
*/
func (c *Actions) Login(usu *models.Usuario) revel.Result {
	pagina := revel.AppName + " - Login"
	fecha := time.Now().Year()
	for k := range c.Session {
		delete(c.Session, k)
	}
	return c.Render(fecha, pagina)
}

/*
	Esta funcion se encarga de hacer el login del usuario
*/
func (c *Actions) LoginNew(usu *models.Usuario) revel.Result {
	// Datos del formulario
	decoder := form.NewDecoder()
	decoder.Decode(usu, c.Params.Form)

	// validacion
	err := usu.ValidarInicioSesion(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect((*Actions).Login)
	} else if err != nil {
		c.Flash.Error("Error al iniciar sesion, los datos no coinciden!")
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect((*Actions).Login)
	}

	c.Session["usuario"] = usu.Usuario
	c.Session["email"] = usu.Email
	c.Session["perfil"] = usu.IDPerfil
	c.Session.SetDefaultExpiration()
	if usu.Foto != nil {
		fotoC := &http.Cookie{Name: revel.AppName + "_foto", Value: *usu.Foto}
		c.SetCookie(fotoC)
	}
	c.Flash.Success("Bienvenido, " + usu.Usuario)
	return c.Redirect((*User).Home)
}

/*
	Este metodo se usa paraa cerrar la sesion de un usuario
*/
func (c *Actions) Logout() revel.Result {
	for k := range c.Session {
		delete(c.Session, k)
	}
	return c.Redirect((*App).Index)
}
