package controllers

import (
	"time"

	"github.com/revel/revel"
)

type User struct {
	*revel.Controller
}

/*
	Esta funcion retorna el index de la pagina
*/
func (c User) Home() revel.Result {
	pagina := revel.AppName + " - Inicio"
	fecha := time.Now().Year()
	return c.Render(fecha, pagina)
}
